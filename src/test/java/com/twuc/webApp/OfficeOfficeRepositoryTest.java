package com.twuc.webApp;

import com.twuc.webApp.dao.OfficeRepository;
import com.twuc.webApp.entity.Office;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ALL")
@DataJpaTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class OfficeOfficeRepositoryTest {
    @Autowired
    OfficeRepository repository;
    @Autowired
    EntityManager em;
    @Test
    void hello_world() {
        //Arrange

        //Act

        //Assert
        assertTrue(true);
    }

    @Test
    void test_select_all() {
        //Arrange
        ArrayList<Office> offices = new ArrayList<>();
        //Act
        List<Office> all = repository.findAll();
        //Assert
        Assertions.assertIterableEquals(offices,all);
    }

    @Test
    void test_save_return_nut_null() {
        //Arrange
        Office office = new Office(1L,"xian");
        //Act
        Office save = repository.saveAndFlush(office);
        //Assert
        assertNotNull(save);
    }
    @Test
    void test_em_return_obj() {
        //Arrange
        Office office = new Office("xian");
        //Act
        em.persist(office);
        em.flush();
        em.clear();
        Office res = em.find(Office.class, 1L);
        //Assert
        assertEquals(Long.valueOf(1),res.getId());
        assertEquals("xian",res.getCity());

    }
}
