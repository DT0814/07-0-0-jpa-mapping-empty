package com.twuc.webApp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author tao.dong
 */
@Entity
public class Office {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String city;

    public Office() {
    }

    public Office(Long id, String city) {
        this.id = id;
        this.city = city;
    }

    public Office(String city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
