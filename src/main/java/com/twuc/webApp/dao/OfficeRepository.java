package com.twuc.webApp.dao;

import com.twuc.webApp.entity.Office;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tao.dong
 */
public interface OfficeRepository extends JpaRepository<Office,Long> {
}
